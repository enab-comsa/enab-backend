FROM php:7.4-fpm-alpine
RUN docker-php-ext-install mysqli pdo pdo_mysql



# RUN apt-get update -y && apt-get install -y libpng-dev

# RUN apt-get update && \
#     apt-get install -y \
#         libjpeg62-turbo-dev \
#         libfreetype6-dev \
#         libjpeg-dev \
#         zlib1g-dev \
#         libonig-dev

# RUN docker-php-ext-install mbstring

# RUN docker-php-ext-install -j$(nproc) gd


COPY /config/php/php.ini /usr/local/etc/php/conf.d