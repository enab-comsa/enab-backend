<?php
namespace App\Http\Controllers;

class TapPayment {

    protected $REQUIRED_CHARGE_VARS = [
        'customer' => [
            'first_name' => true,'middle_name' => false,'last_name' => false,'email' => false,
            'phone' => [
                'country_code' => false,'number' => false
            ]
        ],
        'address' => [
            'country' => false,'city' => false,'line1' => false,'ip' => false
        ],
        'amount' => true,'currency' => true,'save_card' => false,'threeDSecure' => true,'description' => true,'statement_descriptor' => false,
        'metadata' => [
            'udf1' => false,'udf2' => false
        ],
        'reference' => [
            'transaction' => false,'order' => false
        ],
        'receipt' => [
            'email' => false,'sms' => false
        ],
        'merchant' => [
            'id' => false
        ],
        'source'=>[
            'id' => false
        ],
        'post' => [
            'url' => false
        ],
        'redirect'=>[
            'url' => true
        ]
    ];
    protected $CHARGE_VARS = [
        'customer' => [
            'first_name' => null,'middle_name'=>null,'last_name'=>null,'email'=>null,
            'phone'=> [
                'country_code' => null, 'number' => null
            ]
        ],
        'address' => [
            'country' => null,'city' => null,'line1' => null,'ip' => null
        ],
        'amount' => null,'currency' => null,'save_card' => 'false','description' => null,'threeDSecure' => 'true','statement_descriptor' => null,
        'metadata' => [
            'udf1' => null,'udf2' => null
        ],
        'reference' => [
            'transaction' => null,'order' => null
        ],
        'receipt' => [
            'email'=>'true','sms' => 'true'
        ],
        'merchant' => [
            'id' => null
        ],
        'source'=>[
            'id' => null
        ],
        'post' => [
            'url' => null
        ],
        'redirect'=>[
            'url'
        ]
    ];

    protected $REQUIRED_CONFIG_VARS = ['secret_api_Key'=>true];
    protected $CONFIG_VARS = ['secret_api_Key'=>null];

    public function __construct($config = []){
        foreach ($this->REQUIRED_CONFIG_VARS as $parm => $req_status) {
            if (key_exists($parm,$config)) {
                $this->CONFIG_VARS[$parm] = $config[$parm];
            }else{
                if($req_status){
                    throw new \InvalidArgumentException("InvalidArgumentException $parm field");
                }
            }
        }
    }

    protected function chargeValidator($data){
        foreach ($this->REQUIRED_CHARGE_VARS as $Firstkey => $req_status) {
            if (is_array($req_status)) {
            $SecondArray = $this->REQUIRED_CHARGE_VARS[$Firstkey];
            foreach ($SecondArray as $Secondkey => $req_status2) {
                if (is_array($req_status2)) {
                    $ThirdArray = $this->REQUIRED_CHARGE_VARS[$Firstkey][$Secondkey];
                    foreach ($ThirdArray as $Thirdkey => $req_status3) {
                        if (isset($data[$Firstkey][$Secondkey]) &&  key_exists($Thirdkey,$data[$Firstkey][$Secondkey])) {
                            $this->CHARGE_VARS[$Firstkey][$Secondkey] = $data[$Firstkey][$Secondkey];
                        }else{
                            if($req_status3){
                                // missing required parm
                                throw new \InvalidArgumentException("InvalidArgumentException $Firstkey.$Secondkey.$Thirdkey required");
                            }else{
                                if (in_array($Thirdkey,['country_code','number']) && $this->CHARGE_VARS[$Firstkey][$Secondkey][$Thirdkey] == null ) {
                                    if (!isset($this->CHARGE_VARS['customer']['email']) || isset($this->CHARGE_VARS['customer']['email']) && $this->CHARGE_VARS['customer']['email'] == null) {
                                        throw new \InvalidArgumentException("InvalidArgumentException $Firstkey.phone or $Firstkey.email is required");
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if (isset($data[$Firstkey]) && key_exists($Secondkey,$data[$Firstkey])) {
                        $this->CHARGE_VARS[$Firstkey][$Secondkey] = $data[$Firstkey][$Secondkey];
                    }else{
                        if($req_status2){
                            // missing required parm
                            throw new \InvalidArgumentException("InvalidArgumentException $Firstkey.$Secondkey required");
                        }
                    }
                }
            }
            }else{
                if (key_exists($Firstkey,$data)) {
                    $this->CHARGE_VARS[$Firstkey] = $data[$Firstkey];
                }else{
                    if($req_status){
                        // missing required parm
                        throw new \InvalidArgumentException("InvalidArgumentException $Firstkey field");
                    }
                }
            }
        }
    }

    public function charge($data = []){
        $this->chargeValidator($data);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.tap.company/v2/charges",
            CURLOPT_RETURNTRANSFER => true,
            
            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\"amount\":".$this->CHARGE_VARS['amount'].",\"currency\":\"".$this->CHARGE_VARS['currency']."\",\"threeDSecure\":".$this->CHARGE_VARS['threeDSecure'].",\"save_card\":".(string)$this->CHARGE_VARS['save_card'].",\"description\":\"".$this->CHARGE_VARS['description']."\",
                \"statement_descriptor\":\"".$this->CHARGE_VARS['statement_descriptor']."\",\"metadata\":{\"udf1\":\"".$this->CHARGE_VARS['metadata']['udf1']."\",
                \"udf2\":\"".$this->CHARGE_VARS['metadata']['udf2']."\"},\"reference\":{\"transaction\":\"".$this->CHARGE_VARS['reference']['transaction']."\",
                \"order\":\"".$this->CHARGE_VARS['reference']['order']."\"},\"receipt\":{\"email\":".$this->CHARGE_VARS['receipt']['email'].",
                \"sms\":".$this->CHARGE_VARS['receipt']['sms']."},\"customer\":{\"first_name\":\"".$this->CHARGE_VARS['customer']['first_name']."\",
                \"middle_name\":\"".$this->CHARGE_VARS['customer']['middle_name']."\",\"last_name\":\"".$this->CHARGE_VARS['customer']['last_name']."\",
                \"email\":\"".$this->CHARGE_VARS['customer']['email']."\",\"phone\":{\"country_code\":\"".$this->CHARGE_VARS['customer']['phone']['country_code']."\",
                \"number\":\"".$this->CHARGE_VARS['customer']['phone']['number']."\"}},\"merchant\":{\"id\":\" ".$this->CHARGE_VARS['merchant']['id']."\"},
                \"source\":{\"object\":\"token\",\"id\":\"".$this->CHARGE_VARS['source']['id']."\"},\"post\":{\"url\":\"".$this->CHARGE_VARS['post']['url']."\"},
                \"redirect\":{\"url\":\"".$this->CHARGE_VARS['redirect']['url']."\"}}",
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer ".$this->CONFIG_VARS['secret_api_Key']." ",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new \Exception("Exception  $err");
        } else {
            $json_response = json_decode($response);
            if (isset($json_response->errors) && is_array($json_response->errors) && count($json_response->errors) > 0) {
                throw new \Exception("Error : ".json_encode($json_response)." ");
            }
            if (isset($json_response->object) && $json_response->object == "charge" && isset($json_response->transaction->url)) {
                return redirect($json_response->transaction->url);
            }else{
                throw new \Exception("Error : ".$json_response." ");
            }
        }
    }

    public function getCharge($charge_id){
        if ($charge_id != null) {
          $curl = curl_init();
          curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.tap.company/v2/charges/$charge_id",
            CURLOPT_RETURNTRANSFER => true,

            CURLOPT_SSL_VERIFYPEER => false,

            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
            CURLOPT_HTTPHEADER => array(
              "authorization: Bearer ".$this->CONFIG_VARS['secret_api_Key']." ",
            ),
          ));
    
          $response = curl_exec($curl);
          $err = curl_error($curl);
    
          curl_close($curl);
    
          if ($err) {
            throw new \Exception("Exception  $err");
          } else {
            $json_response = json_decode($response);
            if (isset($json_response->errors) && is_array($json_response->errors) && count($json_response->errors) > 0) {
              throw new \Exception("Error : ".$json_response->errors[0]->code." ");
            }
            if (isset($json_response->object) && $json_response->object == "charge" && isset($json_response->id)) {
              return $json_response;
            }else{
              throw new \Exception("Error : ".$response." ");
            }
          }
        }
        return false;
      }    
}