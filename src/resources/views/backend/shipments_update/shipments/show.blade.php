@extends('backend.layouts.app')

@section('content')

<div class="card">
    <div class="card-header">
        <h1 class="h2 fs-16 mb-0">{{ translate('Order Details') }}</h1>
    </div>
    <div class="card-header row gutters-5">
        <div class="col text-center text-md-left">
        </div>
        @php
        $delivery_status = $order->orderDetails->first()->delivery_status;
        $payment_status = $order->orderDetails->first()->payment_status;
      $reason_status = $order->orderDetails->first()->reason_status;
      $Courier_status = $order->orderDetails->first()->Courier_status;
        @endphp
        <div class="col-md-3 ml-auto">
            <label for=update_payment_status"">{{translate('Payment Status')}}</label>
            <select class="form-control aiz-selectpicker"  data-minimum-results-for-search="Infinity" id="update_payment_status">
                <option value="paid" @if ($payment_status == 'paid') selected @endif>{{translate('Paid')}}</option>
                <option value="unpaid" @if ($payment_status == 'unpaid') selected @endif>{{translate('Unpaid')}}</option>
                <option value="COD" @if ($payment_status == 'COD') selected @endif>{{translate('COD')}}</option>
                <option value="by_fida" @if ($payment_status == 'by_fida') selected @endif>{{translate('by fida')}}</option>

            </select>
        </div>
      <div class="col-md-3 ml-auto">
            <label for=update_delivery_status"">{{translate('Delivery Status')}}</label>
            <select class="form-control aiz-selectpicker"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                <option value="pending" @if ($delivery_status == 'pending') selected @endif>{{translate('HOLDING')}}</option>
                <option value="confirmedWH" @if ($delivery_status == 'confirmedWH') selected @endif>{{translate('CONFIRMATION FROM W/H')}}</option>
                 <option value="callcenterconfirm" @if ($delivery_status == 'callcenterconfirm') selected @endif>{{translate('call center confirm')}}</option>
                 <option value="printthebillandawb" @if ($delivery_status == ' printthebillandawb') selected @endif>{{translate('print the bill and awb')}}</option>
                <option value="updateshelve" @if ($delivery_status == 'updateshelve') selected @endif>{{translate('update shelve (comment)')}}</option>
                <option value="updateshelvepackaging" @if ($delivery_status == 'updateshelvepackaging') selected @endif>{{translate('update shelve packaging')}}</option>
                <option value="updatesortingarea" @if ($delivery_status == 'updatesortingarea') selected @endif>{{translate('update sorting area')}}</option>
             <option value="on_delivary" @if ($delivery_status == 'on_delivary') selected @endif>{{translate('ready for delivary')}}</option>
             <option value="ofdoutfodelivery" @if ($delivery_status == 'ofdoutfodelivery') selected @endif>{{translate('ofd out for delivery')}}</option>
                <option value="appcouriers" @if ($delivery_status == 'appcouriers') selected @endif>{{translate('app couriers ( D ship - not D ship)')}}</option>
                <option value="panidingshipments" @if ($delivery_status == 'panidingshipments') selected @endif>{{translate('paniding shipments')}}</option>
                <option value="rto_returnfrom_couriers" @if ($delivery_status == 'rto_returnfrom_couriers') selected @endif>{{translate('rto return from couriers')}}</option>
                <option value="cod_amount" @if ($delivery_status == 'cod_amount') selected @endif>{{translate('cod amount')}}</option>
                <option value="cancelled" @if ($delivery_status == 'cancelled') selected @endif>{{translate('Cancel')}}</option>

            </select>
        </div>
                  
        <br>
              <div class="col-md-3 ml-auto">
            <label for=update_reason_status"">{{translate('reason app couriers')}}</label>
            <select class="form-control aiz-selectpicker"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                <option value="coustomer_want_to_open_package" @if ($reason_status == 'coustomer_want_to_open_package') selected @endif>{{translate('coustomer want to open package')}}</option>
                <option value="Reschedule" @if ($reason_status == 'Reschedule') selected @endif>{{translate('Reschedule futuer (date)')}}</option>
                <option value="Reschedule_Tomorrow" @if ($reason_status == 'Reschedule_Tomorrow') selected @endif>{{translate('Reschedule Tomorrow')}}</option>
                <option value="cancel_Reason" @if ($reason_status == 'cancel_Reason') selected @endif>{{translate('cancel   (Reason)')}}</option>
                <option value="wrong_area_date" @if ($reason_status == 'wrong_area_date') selected @endif>{{translate('wrong area (date)')}}</option>
                <option value="No_response" @if ($reason_status == 'No_response') selected @endif>{{translate('No response')}}</option>
             

            </select>
        </div>
        <br>
                <div class="col-md-3 ml-auto">
            <label for=update_scan_status"">{{translate('scan w/h')}}</label>
            <select class="form-control aiz-selectpicker"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                <option value="pending" @if ($delivery_status == 'pending') selected @endif>{{translate('Pending')}}</option>
             

            </select>
        </div>
        <br>
                <div class="col-md-3 ml-auto">
            <label for=update_Courier_status"">{{translate('Courier')}}</label>
            <select class="form-control aiz-selectpicker"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                <option value="ofd" @if ($Courier_status == 'ofd') selected @endif>{{translate('ofd')}}</option>
                <option value="rto" @if ($Courier_status == 'rto') selected @endif>{{translate('rto')}}</option>
             

            </select>
        </div>
                 <div class="col-md-3 ml-auto">
            <label for=update_CallCenter_status"">{{translate('Call Center')}}</label>
            <select class="form-control aiz-selectpicker"  data-minimum-results-for-search="Infinity" id="update_delivery_status">
                <option value="pending" @if ($delivery_status == 'pending') selected @endif>{{translate('Pending')}}</option>
             

            </select>
        </div>
    </div>
    <div class="card-body">
        <div class="row gutters-5">
            <div class="col text-center text-md-left">
                <address>
                    <strong class="text-main">{{ json_decode($order->shipping_address)->name }}</strong><br>
                    {{ json_decode($order->shipping_address)->email }}<br>
                    {{ json_decode($order->shipping_address)->phone }}<br>
                    {{ json_decode($order->shipping_address)->address }}, {{ json_decode($order->shipping_address)->city }}, {{ json_decode($order->shipping_address)->postal_code }}<br>
                    {{ json_decode($order->shipping_address)->country }}
                </address>
                @if ($order->manual_payment && is_array(json_decode($order->manual_payment_data, true)))
                <br>
                <strong class="text-main">{{ translate('Payment Information') }}</strong><br>
                {{ translate('Name') }}: {{ json_decode($order->manual_payment_data)->name }}, {{ translate('Amount') }}: {{ single_price(json_decode($order->manual_payment_data)->amount) }}, {{ translate('TRX ID') }}: {{ json_decode($order->manual_payment_data)->trx_id }}
                <br>
                <a href="{{ uploaded_asset(json_decode($order->manual_payment_data)->photo) }}" target="_blank"><img src="{{ uploaded_asset(json_decode($order->manual_payment_data)->photo) }}" alt="" height="100"></a>
                @endif
            </div>
            <div class="col-md-4 ml-auto">
                <table>
                    <tbody>
                        <tr>
                            <td class="text-main text-bold">{{translate('Order #')}}</td>
                            <td class="text-right text-info text-bold">	{{ $order->code }}</td>
                        </tr>
                        <tr>
                            <td class="text-main text-bold">{{translate('Order Status')}}</td>
                            @php
                            $status = $order->orderDetails->first()->delivery_status;
                            @endphp
                            <td class="text-right">
                                @if($status == 'delivered')
                                <span class="badge badge-inline badge-success">{{ translate(ucfirst(str_replace('_', ' ', $status))) }}</span>
                                @else
                                <span class="badge badge-inline badge-info">{{ translate(ucfirst(str_replace('_', ' ', $status))) }}</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="text-main text-bold">{{translate('Order Date')}}	</td>
                            <td class="text-right">{{ date('d-m-Y h:i A', $order->date) }}</td>
                        </tr>
                        <tr>
                            <td class="text-main text-bold">
                                {{translate('Total amount')}}
                            </td>
                            <td class="text-right">
                                {{ single_price($order->grand_total) }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-main text-bold">{{translate('Payment method')}}</td>
                            <td class="text-right">{{ ucfirst(str_replace('_', ' ', $order->payment_type)) }}</td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
        <hr class="new-section-sm bord-no">
        <div class="row">
            <div class="col-lg-12 table-responsive">
                <table class="table table-bordered aiz-table invoice-summary">
                    <thead>
                        <tr class="bg-trans-dark">
                            <th data-breakpoints="lg" class="min-col">#</th>
                            <th width="10%">{{translate('Photo')}}</th>
                            <th class="text-uppercase">{{translate('Description')}}</th>
                            <th data-breakpoints="lg" class="text-uppercase">{{translate('Delivery Type')}}</th>
                            <th data-breakpoints="lg" class="min-col text-center text-uppercase">{{translate('Qty')}}</th>
                            <th data-breakpoints="lg" class="min-col text-center text-uppercase">{{translate('Price')}}</th>
                            <th data-breakpoints="lg" class="min-col text-right text-uppercase">{{translate('Total')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($order->orderDetails as $key => $orderDetail)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>
                                @if ($orderDetail->product != null)
                                <a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank"><img height="50" src="{{ uploaded_asset($orderDetail->product->thumbnail_img) }}"></a>
                                @else
                                <strong>{{ translate('N/A') }}</strong>
                                @endif
                            </td>
                            <td>
                                @if ($orderDetail->product != null)
                                <strong><a href="{{ route('product', $orderDetail->product->slug) }}" target="_blank" class="text-muted">{{ $orderDetail->product->getTranslation('name') }}</a></strong>
                                <small>{{ $orderDetail->variation }}</small>
                                @else
                                <strong>{{ translate('Product Unavailable') }}</strong>
                                @endif
                            </td>
                            <td>
                                @if ($orderDetail->shipping_type != null && $orderDetail->shipping_type == 'home_delivery')
                                {{ translate('Home Delivery') }}
                                @elseif ($orderDetail->shipping_type == 'pickup_point')

                                @if ($orderDetail->pickup_point != null)
                                {{ $orderDetail->pickup_point->getTranslation('name') }} ({{ translate('Pickup Point') }})
                                @else
                                {{ translate('Pickup Point') }}
                                @endif
                                @endif
                            </td>
                            <td class="text-center">{{ $orderDetail->quantity }}</td>
                            <td class="text-center">{{ single_price($orderDetail->price/$orderDetail->quantity) }}</td>
                            <td class="text-center">{{ single_price($orderDetail->price) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix float-right">
            <table class="table">
                <tbody>
                    <tr>
                        <td>
                            <strong class="text-muted">{{translate('Sub Total')}} :</strong>
                        </td>
                        <td>
                            {{ single_price($order->orderDetails->sum('price')) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="text-muted">{{translate('Tax')}} :</strong>
                        </td>
                        <td>
                            {{ single_price($order->orderDetails->sum('tax')) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="text-muted">{{translate('Shipping')}} :</strong>
                        </td>
                        <td>
                            {{ single_price($order->orderDetails->sum('shipping_cost')) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="text-muted">{{translate('Coupon')}} :</strong>
                        </td>
                        <td>
                            {{ single_price($order->coupon_discount) }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong class="text-muted">{{translate('TOTAL')}} :</strong>
                        </td>
                        <td class="text-muted h5">
                            {{ single_price($order->grand_total) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="text-right no-print">
                <a href="{{ route('invoice.download', $order->id) }}" type="button" class="btn btn-icon btn-light"><i class="las la-print"></i></a>
            </div>
        </div>

    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        $('#update_delivery_status').on('change', function(){
            var order_id = {{ $order->id }};
            var status = $('#update_delivery_status').val();
            $.post('{{ route('orders.update_delivery_status') }}', {
                _token:'{{ @csrf_token() }}',
                order_id:order_id,
                status:status
            }, function(data){
                AIZ.plugins.notify('success', '{{ translate('Delivery status has been updated') }}');
            });
        });

        $('#update_payment_status').on('change', function(){
            var order_id = {{ $order->id }};
            var status = $('#update_payment_status').val();
            $.post('{{ route('orders.update_payment_status') }}', {_token:'{{ @csrf_token() }}',order_id:order_id,status:status}, function(data){
                AIZ.plugins.notify('success', '{{ translate('Payment status has been updated') }}');
            });
        });
    </script>
@endsection
